output "internal_instance_ip" {
  value = yandex_compute_instance.this.network_interface.0.ip_address
}

output "external_instance_ip" {
  value = yandex_compute_instance.this.network_interface.0.nat_ip_address
}
