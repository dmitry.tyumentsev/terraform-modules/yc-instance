variable "instance_name" {
  description = "Instance resource name"
  type        = string
}

variable "instance_vpc_subnet_id" {
  description = "Subnet vpc id for the instance"
  type        = string
}

variable "is_instance_public" {
  description = "Is the instance supposed to have an external ip"
  type        = bool
  default     = true
}

variable "instance_hostname" {
  description = "Instance resource hostname"
  default     = "instance"
  type        = string
}

variable "instance_labels" {
  description = "A set of key/value label pairs to assign to the instance."
  default     = {}
  type        = map(string)
}

variable "instance_image_family" {
  description = "Distro image family for the instance"
  default     = "debian-10"
  type        = string
}

variable "instance_platform_id" {
  description = "Available processors: https://cloud.yandex.com/en-ru/docs/compute/concepts/vm-platforms"
  type        = string
  default     = "standard-v3"
}

variable "instance_cores" {
  description = "Amount of CPU cores for the instance"
  default     = 2
  type        = number
}

variable "instance_memory" {
  description = "Amount of RAM, GiB for the instance"
  default     = 2
  type        = number
}

variable "instance_core_fraction" {
  description = "Guaranteed share of vCPU % for the instance"
  default     = 100
  type        = number
}

variable "instance_gpus" {
  description = "Number of GPU devices for the instance"
  default     = 0
  type        = number
}

variable "is_instance_preemptible" {
  description = "Specifies if the instance is preemptible"
  default     = false
  type        = bool
}

variable "is_instance_serial_port_enabled" {
  description = "Flag allowing access to the serial console"
  default     = true
  type        = bool
}

variable "instance_disk_type" {
  description = "Instance disk type"
  type        = string
  default     = "network-hdd"
}

variable "instance_disk_size" {
  description = "Instance disk size, GiB"
  type        = number
  default     = 11
}

variable "instance_ssh_user" {
  description = "Instance username for ssh access"
  type        = string
  default     = "ansible"
}

variable "instance_ssh_public_key_path" {
  description = "Instance ssh public key path for ssh access"
  type        = string
  default     = "~/.ssh/id_ed25519.pub"
}

variable "cloud_config_config_path" {
  description = "Cloud config file path for the instance"
  type        = string
  default     = ""
}

variable "public_address" {
  description = "Public address to access the internet over NAT. Address should be already reserved"
  type        = string
  default     = ""
}

variable "private_address" {
  description = "The private IP address to assign to the instance. If empty, the address will be automatically assigned from the specified subnet"
  type        = string
  default     = ""
}

variable "service_account_id" {
  description = "ID of the service account authorized for this instance"
  type        = string
  default     = ""
}
